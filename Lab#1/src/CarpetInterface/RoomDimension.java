/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CarpetInterface;

/**
 *
 * @author kelsey.pritsker676
 */


public class RoomDimension {
    
    private double length;
    private double width;
    
    public RoomDimension(){
        this.length = 1;
        this.length = 1;
    }
    
    public RoomDimension(double len, double w){
        this.length = len;
        this.width = w;
    }
    
    public double getArea(){
        double area = length*width;
        return area;
    }
    
    public String toString(){
        return (length + "ft x " + width + "ft");
    }
    
}
